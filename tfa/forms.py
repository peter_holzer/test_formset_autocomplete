from django import forms
from django.forms.models import inlineformset_factory

from .models import Book, Person, Role, Creator

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        exclude = []

class CreatorForm(forms.ModelForm):
    class Meta:
        model = Creator
        exclude = []

CreatorFormset = inlineformset_factory(Book, Creator, exclude=[])
