from django.db import models

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=80)

    def __str__(self):
        return self.title

class Person(models.Model):
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name


class Role(models.Model):
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name


class Creator(models.Model):
    book = models.ForeignKey(Book)
    person = models.ForeignKey(Person)
    role = models.ForeignKey(Role)

    def __str__(self):
        return self.person.name + " is " + self.role.name + " of " + self.book.title
