from django.shortcuts import render
from django.views.generic import CreateView

from .forms import BookForm, CreatorFormset
from .models import Book

# Create your views here.
class BookCreateView(CreateView):
    form_class = BookForm
    template_name = 'tfa/book_create.html'
    model = Book

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        creator_formset = CreatorFormset()

        return self.render_to_response(
                        self.get_context_data(
                                form=form,
                                creator_formset=creator_formset,
                             )
                    )
