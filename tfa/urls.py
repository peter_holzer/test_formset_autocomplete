from django.conf.urls import url

from .views import BookCreateView

app_name = 'tfa'

urlpatterns = [
    url(r'^create/$', BookCreateView.as_view(), name='create'),
]
