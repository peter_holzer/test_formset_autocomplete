from django.apps import AppConfig


class TfaConfig(AppConfig):
    name = 'tfa'
